#include <Python.h>//include the "Python.h" header before any other include
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <vector>
#include <zlib.h>
#include <errno.h>
#include "def_PyC.h"
#include "Population.h"
#include "Individual.h"
#include "Node.h"

using std::vector;

// Name for the cpp object "capsules"
#define NAME_CAPSULE_POP "POPULATION_"
#define NAME_CAPSULE_IND "INDIVIDUAL_"
#define NAME_CAPSULE_NODE "NODE_"


// Receives a Python capsule for object Population, and extracts the pointer of the C++ object
static Population* PopPythonToC(PyObject* args){
	Population* my_Pop;
	PyObject* capsule;
	if (!PyArg_ParseTuple(args, "O", &capsule)){
		return NULL;
	}
	my_Pop = (Population*) PyCapsule_GetPointer(capsule,NAME_CAPSULE_POP);
	return my_Pop;
}

//DESTRUCTOR
// Frees object Population Python capsule
void PopCapsuleDestructor(PyObject* capsule){
	Population* my_Pop = (Population*) PyCapsule_GetPointer(capsule,NAME_CAPSULE_POP);
  delete my_Pop;
}



 //Calls the Print function of object Population
static PyObject*  PrintPop(PyObject* self, PyObject* args){
    Population*  my_Pop = PopPythonToC(args);
    my_Pop->Print();
    Py_INCREF(Py_None);
    return Py_None;
}

//Creates a C++ vector of booleans given a Python list of booleans
static vector<bool> CPyGetItems(PyListObject* list){
	vector<bool> vect_of_bool;
	// get the size of the list
	int size = PyList_Size((PyObject*) list);
	// iterate over the python list
	for (int j = 0; j < size; j++){
		// get the j-th element of the python list and convert it to long
		bool listItem=  (bool) PyObject_IsTrue(PyList_GetItem((PyObject*) list , (Py_ssize_t) j ));
		vect_of_bool.push_back(listItem);
	}
	for(size_t k =0; k<vect_of_bool.size();++k){
	}
	return vect_of_bool;
}


//Creates a C++ vector of vector of booleans given a Python list of list of booleans
static vector<vector<bool>> CPyGetItemsListOfList(PyListObject** list){
	vector<vector<bool>> matrix_of_bool;
	// get the size of the list
	int size = PyList_Size((PyObject*) list);
	// iterate over the python list
	for (int j = 0; j < size; j++){
	  vector<bool> insert_vect={};
		// get the j-th element of the python list and convert it to long
		PyObject* listItem = (PyObject*) PyList_GetItem( (PyObject*) list , (Py_ssize_t) j);
		int size_sublist=PyList_Size((PyObject*) listItem);
		for(int k=0; k<size_sublist;++k){
		  //Get the kth element of the sublist and converts It into a boolean item in Python
		  bool element = (bool) PyObject_IsTrue(PyList_GetItem((PyObject*) listItem , (Py_ssize_t) k ));
		  insert_vect.push_back(element); // inserts the element in the subvector
		}
		matrix_of_bool.push_back(insert_vect); // Inserts the subvector in the vector

	}
	return matrix_of_bool;
}


// Receive and parse parameters, constructs an object Population, encapsulate it and return the capsule
static PyObject* PopTranslator(PyObject* self, PyObject* args){
	int nb_children;
	int nb_var;
	float threshold;
	int nb_generations_user;
	PyListObject* list_bool;
	PyListObject** matrix_bool;
	if (!PyArg_ParseTuple(args, "iifiOO", &nb_children, &nb_var,&threshold,&nb_generations_user,&matrix_bool,&list_bool)){
		return NULL;
	}
	vector<vector<bool>> new_matrix=CPyGetItemsListOfList(matrix_bool);
 	vector<bool> new_obs=CPyGetItems(list_bool);
	Population* my_Pop = new Population(nb_children, nb_var,threshold, nb_generations_user,new_matrix,new_obs);
	PyObject* capsule = PyCapsule_New(my_Pop, NAME_CAPSULE_POP, PopCapsuleDestructor);
	return capsule;
}


//run simulation

static PyObject* run(PyObject* self, PyObject* args){
  /**
  Run the simulation and find the best individual
  **/
  Population*  my_pop = PopPythonToC(args);
  my_pop->final_indiv();
  Py_INCREF(Py_None);
  return Py_None;
}

// Receive and parse parameters, constructs an object Node, encapsulate it and return the capsule
static PyObject* FitnessBestIndiv(PyObject* self, PyObject* args){
    Population*  my_pop = PopPythonToC(args);
    float fit = my_pop->fitness();
    return Py_BuildValue("f", fit);
}

//Gets the formula of the best individual
static PyObject* FormulaBestIndiv(PyObject* self, PyObject* args){
  Population*  my_pop = PopPythonToC(args);
  //my_pop->final_indiv();
  string best_formula=my_pop->formula();
  return Py_BuildValue("s",best_formula.c_str());
}

//Gets the range of the best individual
static PyObject* RangeBestIndiv(PyObject* self, PyObject* args){
  Population*  my_pop = PopPythonToC(args);
  int i=my_pop->convergence_fitness();
  return Py_BuildValue("i",i);
}



//Creates a Python List of lfoats, given the vector of floats of the individuals' fitnesses
static PyObject* EvolutionFitness(PyObject* self, PyObject* args){
  Population*  my_pop = PopPythonToC(args); // Creation of a capsule Population
  vector<float> evolution=my_pop->evolution_fitness();
  size_t list_size= evolution.size();
  PyObject* list = PyList_New(list_size); // Allocation of memory for the Python list, with the size of evoltion
  for (size_t i=0; i<list_size; ++i){
    PyList_SetItem(list, (Py_ssize_t)i, Py_BuildValue("f", evolution[i])); // copy og the elements of evolution in the list
  }

  return list;
}


//Creates a Python List of lfoats, given the vector of floats of the individuals' fitnesses
static PyObject* vect_compute_value(PyObject* self, PyObject* args){
  Population*  my_pop = PopPythonToC(args); // Creation of a capsule Population
  vector<float> compute_val = my_pop->compute_values_vect();
  size_t list_size= compute_val.size();
  PyObject* list = PyList_New(list_size); // Allocation of memory for the Python list, with the size of evoltion
  for (size_t i=0; i<list_size; ++i){
    PyList_SetItem(list, (Py_ssize_t)i, Py_BuildValue("f", compute_val[i])); // copy og the elements of evolution in the list
  }
  return list;
}



//------------------------------REALS--------------------

//Implementation in case of real numbers of dimension >1
/*
static vector<vector<float>> CPyGetItemsListOfList_reals(PyListObject** list){
	vector<vector<float>> matrix_of_float;
	// get the size of the list
	int size = PyList_Size((PyObject*) list);
	// iterate over the python list
	for (int j = 0; j < size; j++){
	  vector<float> insert_vect={};
		// get the j-th element of the python list and convert it to long
		PyObject* listItem = (PyObject*) PyList_GetItem( (PyObject*) list , (Py_ssize_t) j);
		int size_sublist=PyList_Size((PyObject*) listItem);
		// check PyFloat_AsDouble for doubles
		for(int k=0; k<size_sublist;++k){
		 // bool element = (bool) PyLong_FromLong(PyList_GetItem((PyObject *) listItem, (Py_ssize_t) k)); // check PyFloat_FromDouble for floats
		  float element = (float) PyFloat_FromDouble(PyList_GetItem((PyObject*) listItem , (Py_ssize_t) k ));
		  std::cout<<element<<std::endl;
		  insert_vect.push_back(element);
		}
		matrix_of_float.push_back(insert_vect);
	}
	return matrix_of_float;
}
*/


static vector<float> CPyGetItems_reals(PyListObject* list){
	vector<float> vect_of_float;
	// get the size of the list
	int size = PyList_Size((PyObject*) list);
	// iterate over the python list
	for (int j = 0; j < size; j++){
		// get the j-th element of the python list and convert it to long
		float listItem=  (float) PyFloat_AsDouble(PyList_GetItem((PyObject*) list , (Py_ssize_t) j ));
  	vect_of_float.push_back(listItem);
	}
	return vect_of_float;
}

//Population translator in the case of real numbers
static PyObject* PopTranslator_real(PyObject* self, PyObject* args){
	int nb_children;
	float threshold;
	int nb_generations_user;
	PyListObject* list_float;
	PyListObject* matrix_float;
	if (!PyArg_ParseTuple(args, "ifiOO", &nb_children,&threshold,&nb_generations_user,&matrix_float,&list_float)){
	  return NULL;
	}
	vector<float> new_matrix=CPyGetItems_reals(matrix_float);
	//Vector of observations
	vector<float> new_obs=CPyGetItems_reals(list_float);
	Population* my_Pop = new Population(nb_children,threshold, nb_generations_user,new_matrix,new_obs);
	PyObject* capsule = PyCapsule_New(my_Pop, NAME_CAPSULE_POP, PopCapsuleDestructor);
	return capsule;
}





// Module functions {<python function name>, <function in wrapper>, <parameters flag>, <doctring>}


static PyMethodDef module_funcs[] = {
		{"generate_Pop", (PyCFunction)PopTranslator, METH_VARARGS, "Create an instance of class Population of boolean type\n\n Args: int nb_Children, int nbr_var, float threshold, int nb_generations_user,  const vector<vector<bool>> mat_user, const vector<bool> obs\n\t capsule: Object Population capsule \n Returns:object of type Population"},
		{"fitness_best_indiv", FitnessBestIndiv, METH_VARARGS, "Returns the fitness of the best individual\n\nArgs: object of type Population \n\t capsulePop (Capsule) : object Population capsule \n Returns: float "},
		{"get_formula",FormulaBestIndiv , METH_VARARGS, "Returns the formula of the best individual\n\nArgs: object of type Population \n\t capsulePop (Capsule) : object Population capsule \nReturns: string "},
		{"evolution_fitness",(PyCFunction)EvolutionFitness , METH_VARARGS, " Returns a vectors of floats which are the fitness values of the best individual of each generations\n\nArgs:object of type Population \n\tcapsulePop (Capsule) : object Population capsule \nReturns: list of floats "},
		{"generate_Pop_real", (PyCFunction)PopTranslator_real, METH_VARARGS, "Create an instance of class Population of real type \n\n Args: int nb_Children, float threshold, int nb_generations_user,  const vector<float> vect_var, const vector<float> vect_obsvect_obs_real \n\n capsule: Object Population capsule \nReturns: object of type Population"},
		{"convergence", RangeBestIndiv, METH_VARARGS, "Returns an int which is the index of the generation when the final best individual is found \n\nArgs: object of type Population \n\t capsule: Object Population capsule \nReturns: float"},
    {"print_Pop", PrintPop, METH_VARARGS, "Trivial test that prints'Hello World' \n\n Args: object of type Population\n\t capsulePop (Capsule) : object Population capsule "},
    {"compute_val",(PyCFunction)vect_compute_value , METH_VARARGS, " Returns a vectors of floats which are the compute values of the best individual for the corresponding variables values \n\nArgs:object of type Population \n\t capsulePop (Capsule) : object Population capsule \n Returns: list of floats \n\n"},
    {"run_simulation",(PyCFunction)run, METH_VARARGS, " Run a simulation, searching for the best individual \n\nArgs:object of type Population \n\t capsulePop (Capsule) : object Population capsule"},
		{NULL, NULL, METH_NOARGS, NULL}
};

static struct PyModuleDef moduledef = {
        PyModuleDef_HEAD_INIT,
        "my_wrapper_c",
        "my_wrapper_c module is the C++ extension for python",
        sizeof(PyObject*),
				module_funcs,
				NULL,
        NULL,
        NULL,
        NULL
};

PyMODINIT_FUNC PyInit_my_wrapper_c(void){
    PyObject* module = PyModule_Create(&moduledef);
		return module;
}

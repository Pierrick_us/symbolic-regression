# Symbolic Regression
 The aim of this project is to implement a C++ kernel and a python extension in order to appoximate a set of datas by a function using bases of genetic programming
 
## Targets
* Implement classes in C++
* Introduction to genetic programming
* Manage versioning with the using of Git 
* Learn to code in group
* Use valgrind and other techniques of debugging
* Manage accesses to memory
* Manage a project with AGILE method

## Getting started
* PyWrapper.cpp : Python extension used as an interface via Python with the user
* Individual.cpp Node.cpp and Population (and theri related .h files) : members of the kernel
* main.cpp : file where tests of the C++ kernel can be run
* PyWrapper.cpp : Python extension that casts Python objects using C++ classes (creation of Population object in C++ and casting as a Python Object)
* setup.py : file that permits creation of the wrapper module
* install.sh and compile.sh : files that permits to compile and build the Python API
* script.py : Python file where we can run the simulation and visualize the plots

### Prerequisites

None

### Installing
 The zip file from gitlab has to be downloaded

## Running the tests
To run simulation, open the file script.py and execute It in a text editor such as Spyder. To run the simulation with another dataset, please enter lists of floats or lists of lists just like in the examples
To run tests of C++ kernel (main.cpp) : make (+enter) and ./main commands in the shell


## Versioning
We used GitHub for versioning. A .git file is available with the history of all the commits and merge done by the members of the group.

## Authors
* Armande CIROT : C++ kernel
* Catalina GONZALEZ-GOMEZ :  C++ kernel
* Pierrick ULLIUS : C++ kernel
* Anissa EL MARRAHI : main part of the Python extension

## Aknowledgments
* Sofware development (D. PARSONS) courses + S. PEIGNIER Aknowledgements
* stackOverflow.com and GeeksforGeeks sites as help.
* GitHub community

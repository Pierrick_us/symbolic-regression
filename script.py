import my_wrapper_c as mwc
import matplotlib.pyplot as plt
from sympy import simplify
from pylab import *


def summary(my_x, my_y, real):
    """ 
    Runs a simulation to get the best symbolic regression 
    Args: list variables, list of observations, boolean true if real case false if boolean
    """
    nb_gen = (int(input("How many generations  do you want  ?")))
    nb_children= (int(input("How many children per generation do you want ?")))
    threshold= (float( input ("Please define the threshold of the fitness : ")))

    if (real):
        pop=mwc.generate_Pop_real(nb_children,threshold,nb_gen,my_x,my_y)

        print("\n \t\t Run Simulation :\n" )
        mwc.run_simulation(pop)

        print("Formula of the best individual :", mwc.get_formula(pop))
        
        if (len(mwc.get_formula(pop)) < 75):
            simple_form =  simple_formula(mwc.get_formula(pop))
            print("Symplified formula of the best individual :", simple_form, "\n")
        else:
            simple_form = mwc.get_formula(pop)
            print("Formula too long to be simplified")

        model_val =mwc.compute_val(pop)
        plot_model(my_x, my_y, model_val, simple_form)
        if(mwc.fitness_best_indiv(pop) > threshold):
            print("ATTENTION: the threshold couldn't be reached, try with a greater number of children per generations and/or a greater number of generations")
        print("Fitness of the best individual : ",mwc.fitness_best_indiv(pop))
        print("The best individual was found at generation :", mwc.convergence(pop))

        list_fitness =mwc.evolution_fitness(pop)
        plot_fitness_evol(list_fitness,  mwc.convergence(pop))

    else:
        pop = mwc.generate_Pop(nb_children,len(my_x[0]),threshold,nb_gen,my_x,my_y)
        print("Formula of the best individual :", mwc.get_formula(pop))
        print("Fitness of the best individual : ",mwc.fitness_best_indiv(pop))

    print("\n \t\t End of the Simulation :\n" )



def plot_model(my_x, my_y, model_val, formula):
    """
    Plot the observations and the values predicted by the model
    Args: list of variables, list of observations, list of the values predicted by the model, formula of the model
    Prerequisites: be in the real case (not booleans)
    """
    plt.plot(my_x, my_y, "b:o", label="observations", c='lightblue')
    if(len(formula) < 50):
        plt.plot(my_x, model_val, "b:o", label="model: " +formula, c='coral')
    else:
        plt.plot(my_x, model_val, "b:o", label="model", c='coral')        
    plt.legend()
    plt.title("Plot of the model and observations values")
    plt.xlabel("x")
    plt.ylabel("f(x)")
    plt.show()


def plot_fitness_evol(list_fitness, convergence):
    """
    Plot the evolution of the fitness
    Args: list of the fitness evolution, range since when the fitness converge
    """
    plt.plot(list_fitness)
    plt.axis([0, convergence+5, 0, max(list_fitness[0]/10, min(list_fitness)+10)])
    plt.axvline(x=convergence, ls = '-.', color='red',)
    annotate("Best Indiv", xy = (convergence,15), xytext = (convergence+0.5, 10))
    plt.title("Evolution of the fitness through the generation")
    plt.ylabel('Fitness of the current Best Indiv')
    plt.xlabel('Generation Index')
    plt.show()


def simple_formula(formula):
    """ 
    Simplify the formula of the function
    Args: string of the formula function
    Returns: string of the formula simplified
    """
    return str(simplify(formula))


help(mwc.run_simulation)
help(mwc.get_formula)
help(summary)
help(mwc.generate_Pop)

my_x1=[(-2), (-1), 0, 1.5, 6, 7, 10]
my_y1 =[(-3), (-1), 1, 4, 13, 15, 21]

my_x2=[0.5, 1.5, 6, 9, 11]
my_y2=[2.81, 9, 29.78, 42.59, 50.99]

""" theorical formula : (8*x + 4*ln(3*x) ) /  ( x^(3/2)) """

my_x3 = [0.16, 0.17, 0.18, 0.19, 0.22, 0.24, 0.28, 0.3, 0.4, 0.5, 0.6, 0.7,  0.8, 1, 1.5, 2, 2.8, 4, 6, 10]
my_y3 = [-25.87, -19.02,-13.41,  -8.80, -4.95, 0.95, 5.15, 10.41,  12.04, 15.53, 15.90, 14.63,  13.83, 12.39, 9.80, 8.19,  6.60, 5.24, 4.05, 2.96]

summary(my_x1, my_y1, True)

summary(my_x2, my_y2, True)

summary(my_x3, my_y3, True)



"""
t=[0,20.5,45.0,53,68.5,77,92.5,102.5,115,125.5,140.5,150,164.5,172]
ylog10= [5.89,5.85,6.04,6.18,6.52,6.75,7.38,7.72,8.11,8.38,8.56,8.58,8.76,8.78]
summary(t,ylog10,True)

s=[3.3,3.3,2.5,2.5,2,2,1.6,1.6,1.25,1.25,1,1,0.83,0.83,0.71,0.71,0.625,0.625,0.55,0.55,0.5,0.5]
v=[5.88,6.66,4.76,4.34,3.84,4.34,3.03,2.85,2.77,3.125,2.33,2.33,2.04,2.22,2,1.88,1.64,1.69,1.56,1.53,1.43,1.49]
summary(s,v,True)


pH=[4,4.1,4.3,4.5,4.7,5,6,7,8,9,9.2,9.4,9.6,9.8,10]
mumax=[0,0,0,0,0.112,0.229,0.8,0.93,0.83,0.285,0.232,0,0,0,0]
summary(pH,mumax,True)

my_booleans= [[1,1,1,0],[1,1,1,1],[0,0,1,0]]
my_obs=[1,0,0]



summary(my_booleans, my_obs, False)

"""

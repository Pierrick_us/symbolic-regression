#ifndef NODE_
#define NODE_ 
#include <vector>
#include <string> 

using std::string;

 
class Node{

	friend class Individual; 


public:

 // Constructor - Destructor 
 
	Node()=delete;
	Node(const Node& model) = delete;
	Node(string cont, Node* left, Node* right, Node* prev, int type);
	Node(string cont, Node* prev, int type);

	~Node()=default;

	
	// Methods 
	
	int toInt(string s);
	/**
	Input : a string which is the content of a Node
	Return : an int (negative or positive) which was contained in the string
	**/
 
	
  // Getters 
  
  string get_content();
  
  Node* get_left();

  Node* get_previous();
  
  Node* get_right();
	
  
protected:

	string content_;
	Node* left_;
	Node* right_;
	Node* previous_;
	int type_; // 1-> op simple 2-> op double 3 -> var 4 -> const 
	
};

#endif //NODE_


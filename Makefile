CXXFLAGS=-Wall -O0 -g -Wextra -std=c++11

main: main.o Node.o Individual.o Population.o
	g++ -o main main.o Node.o Individual.o Population.o
	
main.o: main.cpp Node.h Individual.h Population.h
	g++ -o main.o -c $(CXXFLAGS) main.cpp
	
Node.o: Node.cpp Node.h
	g++ -o Node.o -c $(CXXFLAGS) Node.cpp
	
Individual.o: Individual.cpp Individual.h Node.h
	g++ -o Individual.o -c $(CXXFLAGS) Individual.cpp

Population.o: Population.cpp Population.h Individual.h Node.h
	g++ -o Population.o -c $(CXXFLAGS) Population.cpp

clean:
	rm main main.o Node.o Individual.o Population.o

import os
os.environ["CC"] = "c++"
from distutils.core import setup, Extension
module = Extension('my_wrapper_c', ["Population.cpp","Individual.cpp","Node.cpp","PyWrapper.cpp"],include_dirs=[],libraries=[])
module.extra_compile_args = []#,'-pg']

setup(name='my_wrapper_c',
	  version='1.0',
	  author='Anissa EL MARRAHI',
	  author_email='anissa.el-marrahi@insa-lyon.fr',
	  ext_modules=[module])

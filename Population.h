#ifndef POPULATION_
#define POPULATION_

#include "Node.h"
#include "Individual.h"
#include <string>
#include <list>
#include <vector>
#include <iostream>

using std::vector;

class Population{
  
  

  //public:   
  protected:
    
    /***** BOOLEAN OPERATORS - VARAIBLES - CONSTANTS ********/
    
    static vector<string> simple_bool_operators_;     //vector of posible simple operators
    static vector<string> double_bool_operators_;    //vector of posible double operators
    static  vector<string> bool_const_;             //vector of the constants
    
    
        /***** REAL OPERATORS - VARAIBLES - CONSTANTS ********/
        
    static vector<string> simple_real_operators_;     //vector of posible simple operators
    static vector<string> double_real_operators_;    //vector of posible double operators
    static vector<string> real_variables_;          //vector of string references to the  variables
    static vector<string> real_const_;             //vector of the constants
    
    static int nb_generations_max_;
    
    
    bool booleanCase_;
    float threshold_fitness_;           // fitness treshold given by the user
    int nb_generations_user_;            //  max number of generations that can be created
    
    
    int nb_Children_;                   // number of Children of ageneration
    Individual* parent_;                // pointer to the Individual parent of a generation
    
    vector<string> bool_variables_;     //vector of string references to the  variables

    vector<vector<bool>> matrice_user_; // matrices of the variables values (booleans)
    vector<bool> vect_obs_;             // vector of the observation value  (reals)

    vector<float> vect_var_;            // matrices of the variables values (booleans)
    vector<float> vect_obs_real_;            // vector of the observation value (reals)
    
    vector<float> evolution_fitness_;  // vector of float - contains the best fitness of each gen 



    Individual* best_indiv_;
    vector<string> simple_operators_;   //vector of posible simple operators
    vector<string> double_operators_;   //vector of posible double operators
    vector<string> variables_;          //vector of string references to the possible variables
    vector<string> const_;              //vector of the constants
  

  public:

 /************************
 Constructor - Destructor
*************************/

  Population()=delete;
	Population(const Population &model)=delete;
	Population(int nb_Children, int nbr_var, float threshold, int nb_generations_user,  const vector<vector<bool>> mat_user, const vector<bool> obs); // constructor for boolean

  Population(int nb_Children, float threshold, int nb_generations_user,  const vector<float> vect_var, const vector<float> vect_obsvect_obs_real); // Constructor for reals
	~Population();


/***********
  Getters
***********/

  inline int get_nb_Children();
  inline Individual* get_parent();


/***********
  Methods
***********/


  Individual* create_first_ind ();
  /**
  Create randomly the first individual of the population ( parent_)
  return : pointer to the Individual parent_
  **/
  void Print();
 
  void find_best_indiv();


  /**
  Create nb_Child_ children from the parents, for each child created, we compare his fitness with the individual who has the best fitness.
  return:  Adress of the Individual who have the best fitness
  **/


  void final_indiv();

  /**
  Create generation of Individuals until the fitness of one  is acceptable
  return : a pointer to the first Individual created who has a fitness_ < treshold_fitness_ given by the user
  **/

  vector<float> compute_values_vect();
  /**
  return : a vectors of floats which are the compute values of the best individual, calculate with the values of the variable given by the user
  **/
  
  string formula();
  /**
  return a string which is the formula of the current best_indiv_ of the population
  **/
  
  float fitness();
  /**
  return a float which is the fitness of the current best_indiv_ of the population
  **/
 
  vector<float> evolution_fitness();
  /**
  return : a vectors of floats which are the fitness values of the best individual of each generations
  **/
  
  
  int convergence_fitness();
  /**
  return : a int which is the index of the generation when the final best individual is found
  **/
  
};



/***********
  Getters Definition
***********/

int Population::get_nb_Children() {
  return nb_Children_;
}


Individual* Population::get_parent() {
  return parent_;
}





#endif //POPULATION_

#include "Node.h"
#include <string> 
#include <vector>
#include <iostream> 
#include <boost/lexical_cast.hpp>
using std::string; 


/*********** 
 Constructors
***********/


Node::Node(string cont, Node* left, Node* right,Node* prev, int type){
	content_=cont;
	left_=left;
	right_=right;
	previous_=prev;
	type_=type;
}

Node::Node(string cont,Node* prev,int type){
	content_=cont;
	left_=nullptr;
	right_=nullptr;
	previous_=prev;
	type_=type;
}


// Getters

string Node::get_content(){
  return content_;
}

Node* Node::get_left(){
  return left_;
}

Node* Node::get_right(){
  return right_;
}

//Methods 

int Node::toInt(string s)  {  
    int num = 0;  
     
    // Check if the integral value is  negative or not 
    
    // If it is positive, generate the number normally 
    if(s[0] != '-') {
    	std::cout << "positif" << std::endl;
      for (size_t i=0; i<s.length(); i++)  {
      	num = num*10 + (size_t(s[i])-48);  
      }
      
    // If it is negative, calculate the positive number 
    // first ignoring the sign and invert the   sign at the end 
   	} 
   	else {
   	  std::cout << "negatif" << std::endl;
      for (size_t i=1; i<s.length(); i++) { 
        num = num*10 + (size_t(s[i])-48); 
      }
     	num = -num; 
    }
    return num;  
}  




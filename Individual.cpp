#include "Node.h"
#include "Individual.h"

#include <boost/lexical_cast.hpp>
#include <string>
#include <vector>
#include <cstdlib>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cfloat>


using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::stof;


/************
 Constructors
************/

Individual::Individual(Node* start, vector<string> simple, vector<string> doubles, vector<string> variables, vector<string> consts){
  fitness_ = 0;
  root_= start;
  list_nodes_.push_back(start);
  simple_operators_= simple;
  double_operators_= doubles;
  variables_=variables;
  const_=consts;
}


/*****************
 Copy Constructor
*****************/

 Individual::Individual(const Individual &model){

  double_operators_= model.double_operators_;
  simple_operators_= model.simple_operators_;
  variables_ = model.variables_;
  const_ = model.const_;
  root_ = duplicate_Node(model.root_); //
  fill_ListNode(root_);
  fitness_ = model.fitness_;

}

/**********
 Destructor
**********/

Individual::~Individual(){
	delete_Node(0,root_);
	delete root_;
}

/****************
Protected methods
****************/

Node* Individual::duplicate_Node(const Node* model) {
  if (model == nullptr){
   return nullptr;
  }

  Node *newnode = new Node(model->content_, nullptr, nullptr, nullptr, model->type_);
  newnode->left_ = duplicate_Node(model->left_);
  newnode->right_ = duplicate_Node(model->right_);

  if (newnode-> left_ != nullptr){
  newnode-> left_ -> previous_ = newnode;
  }

  if (newnode-> right_ != nullptr){
  newnode-> right_ -> previous_ = newnode;
  }

  return newnode;
}


void Individual::fill_ListNode(Node* n){
  if (n != nullptr){
    list_nodes_.push_back(n);
    fill_ListNode(n->left_);
    fill_ListNode(n->right_);

  }
}

/*************
Public methods
**************/

// Method to copy an Indivual (quite similar of the copy-constructor) :

Individual* Individual::copy_indiv(const Individual* indiv){
  Individual* newIndiv = new Individual(duplicate_Node(indiv ->root_), indiv -> simple_operators_, indiv -> double_operators_, indiv -> variables_, indiv -> const_);
  newIndiv -> list_nodes_.pop_back(); // we erase the root_ because it will be add again in method fill_ListNode()
  newIndiv ->fitness_ = indiv ->fitness_;
  newIndiv -> fill_ListNode(newIndiv -> root_);

  return newIndiv;
}



    /****  Apply a mutation in an Individual  ****/

void Individual::apply_mutation(){
	int random_index= rand()%list_nodes_.size(); //random index from the node that will mutates
	int random_mutation=rand()%3; // random index from the mutation: 0 -> delete 1 -> add 2 -> replace

	if(random_mutation==0){
		delete_Node(random_index,list_nodes_[random_index]);

	}else if(random_mutation==1){
		insert_Node(random_index, list_nodes_[random_index]);

	}else if(random_mutation==2){
		replace_Node(random_index,list_nodes_[random_index]);
	}
}



  /****  The mutation is a Deletion  ****/

// Method to delete a Node in the tree :

void Individual::delete_Node(int pos, Node* old){

	Node* n= new Node("",old->previous_,0);
	list_nodes_.push_back(n);


	if(old->previous_ == nullptr){ // IF we delete the root : we need to update of the root
		root_=n;
	}

	else{
		if(old->previous_->left_==old){
				old->previous_->left_=n;
		}else{
				old->previous_->right_=n;
		}
	}

	int op=rand()%2;
		if(op==0){ //variable
			n->content_=variables_[rand()%variables_.size()];
			n->type_=3;
		}else{ //constant
			n->content_= const_[rand()%const_.size()];
			n->type_=4;
		}

	delete_branch(old);
}


// Method to delete the subtree below a Node

void Individual::delete_branch(Node* n){
	if(n!= nullptr){
		delete_branch(n->left_);
		delete_branch(n->right_);
		list_nodes_.erase(list_nodes_.begin() + find_node(n));
		delete n;
		n=nullptr;
	}else{
		delete n;
		n=nullptr;
	}
}


      /****  The mutation is an Insertion  ****/


// Method to choice a random Nod and insert it in the Individual before the Node old :

void Individual::insert_Node(int pos, Node* old){
	int op=rand()%2;

	Node* n= new Node("",old,nullptr,old->previous_,0); // le left du nouveau node est le node selectioné et le previous du node sélectioné est le nouveau node

	if(old->previous_ == nullptr){ // IF we delete the root : we need to update of the root
		root_=n;
	}

	else{
		if(old->previous_->left_==old){
				old->previous_->left_= n; // le left du previous devient le nouveau node
		}else{
				old->previous_->right_= n; // le right du previous devient le nouveau node
		}
	}

	old -> previous_ = n;

	if(op==0){  // New node is a simple operator
		n->content_=simple_operators_[rand()%simple_operators_.size()];
		n->type_=1;
		list_nodes_.push_back(n);


	}else{ // New node is a double operator  - we need to insert another Node at its right_
		int val=rand()%2;
		Node* n2= new Node("",nullptr,0);

		if(val==0){ // its right_ is a variable
			n2->content_=variables_[rand()%variables_.size()];
			n2->type_=3;
			list_nodes_.push_back(n2);

		}else{ // its right_ is a constant
			n2->content_=const_[rand()%const_.size()];
			n2->type_=4;
			list_nodes_.push_back(n2);
		}

		n->content_=double_operators_[rand()%double_operators_.size()];
		n->right_=n2;
		n->type_=2;
		n2->previous_=n;
		list_nodes_.push_back(n);
	}

}


    /****  The mutation is a Replacement  ****/

// Method to replace a Node :

void Individual::replace_Node(int pos, Node* old){
	int t = old->type_;
	Node* n= new Node("",old->previous_,0);

	//update of the list of nodes
	list_nodes_.erase(list_nodes_.begin() + pos);
	list_nodes_.push_back(n);
	if(old->previous_ == nullptr){ 		//Update of the root if it is replace
		root_=n;
	}
	else{
		if(old->previous_->left_== old){
				old->previous_->left_= n;
		}else{
				old->previous_->right_=n;
		}

	}

	//	OLD NODE IS A SIMPLE OPERATOR
	if(t==1){
		int op=rand()%2;

		if(op==0){ //New Node Simple operator
			n->content_=simple_operators_[rand()%simple_operators_.size()];
			n->type_=1;

		}else{//New Node Double operator
			int val=rand()%2;
			Node* n2= new Node("",nullptr,0);
			if(val==0){
				n2->content_=variables_[rand()%variables_.size()];
				n2->type_=3;
				list_nodes_.push_back(n2);
			}else{
				n2->content_=const_[rand()%const_.size()];
				n2->type_=4;
				list_nodes_.push_back(n2);
			}
			n->content_=double_operators_[rand()%double_operators_.size()];
			n->right_=n2;
			n->type_=2;
			n2->previous_=n;
		}

		n->left_=old->left_;
		old->left_->previous_=n;
	}

	//	OLD NODE IS A DOUBLE OPERATOR
	else if(t==2){
		int op=rand()%2;
		if(op==0){ //New Node Simple operator
			int sup=rand()%2;
			n->content_= simple_operators_[rand()%simple_operators_.size()];
			n->type_=1;
			if(sup==0){ //left node deleted
				delete_branch(old->left_); 
				n->left_=old->right_;
				old->right_->previous_=n;
			}else{
				delete_branch(old->right_); 
				n->left_=old->left_;
				old->left_->previous_=n;
			}
		}else{// New Node Double operator
			n->content_=double_operators_[rand()%double_operators_.size()];
			n->type_=2;
			n->left_=old->left_;
			old->left_->previous_=n;
			n->right_=old->right_;
			old->right_->previous_=n;
		}
	}

	//OLD NODE IS A VARIABLE
	else if(t==3){
		int op=rand()%2;
		if(op==0){ //variable
			n->content_=variables_[rand()%variables_.size()];
			n->type_=3;
		}else{ //constant
			n->content_= const_[rand()%const_.size()];
			n->type_=4;
		}
	}

	//OLD NODE IS A CONSTANT
	else if(t==4){
		int op=rand()%2;
		if(op==0){ //variable
			n->content_=variables_[rand()%variables_.size()];
			n->type_=3;
		}else{ //constant
			n->content_= const_[rand()%const_.size()];
			n->type_=4;
		}
	}

	delete old;
}



/****  Method to print the formula of an Individual  ****/

string Individual::get_formula(Node* rootNode) {
  string s;
	if (rootNode != nullptr){
		if (rootNode -> type_ == 2) {
			s += "(";
			s += get_formula(rootNode -> left_);
			s+= rootNode->content_ ;
			s += get_formula(rootNode -> right_);

		}else if(rootNode -> type_ == 1){
			s += "("; 
			s += rootNode->content_; 
			s += "(";
      s += get_formula(rootNode -> left_);
      s += ")";

		}else if (rootNode -> type_ == 3){
		  	s += get_formula(rootNode -> left_);
			s += rootNode->content_; 
			s += get_formula(rootNode -> right_);
		}

		else {
			s += get_formula(rootNode -> left_);
			s += rootNode->content_; 
			s +=get_formula(rootNode -> right_);
		}
		if (rootNode -> type_ <= 2) {
		  s += ")"; 
 		}
	}
	return s;
}

            /************* Methods for Boolean ******************/

/****  Method to compute the formula an Individual  ****/

bool Individual::compute(Node* aNode, const std::vector<bool> line){

	if ((aNode -> left_ == nullptr) && (aNode -> right_ == nullptr)) { //If the left or right node is empty
	    if (aNode -> type_ == 4){ //If it is a constant, we convert the string in a bool
	        return boost::lexical_cast<bool>(aNode -> content_);
	    }
	    if (aNode -> type_ == 3) { //If it is a variable, we convert it's place in the list in int and get the value given by a line of the matrix
	        int num = 0;
            for (size_t i=0; i<(aNode -> content_).length(); i++){ //converting into int
                num = num*10 + size_t((aNode -> content_)[i]-48);
            }
            return line.at(num);
        }
    }
    else if ((aNode -> left_ != nullptr) && (aNode -> right_ == nullptr)) {
	    bool left_value = compute(aNode ->left_, line);

	    if(aNode -> content_ == "!"){
		    return !left_value;
	    }
	}

	else if ((aNode -> left_ != nullptr) && (aNode -> right_ != nullptr)) {
	    bool left_value = compute(aNode ->left_, line);
	    bool right_value = compute(aNode ->right_, line);

	   if(aNode -> content_ == "&&"){
		    return left_value && right_value;
	    }

	    if(aNode -> content_ == "||"){
		    return left_value || right_value;
	    }
    }
}


void Individual::calculate_Fitness(const std::vector<vector<bool>> matvar, const std::vector<bool> vect){
  fitness_ = 0;

    std::vector<bool> valformula;
    for (size_t i=0; i<matvar.size(); ++i){
        valformula.push_back(compute(root_, matvar.at(i)));
    }
    for (size_t j=0; j<vect.size(); ++j){
      if (vect.at(j) != valformula.at(j)){
          ++fitness_;
      }
    }
}

// Method to find the position of a Node in list_nodes :

int Individual::find_node(Node* item){
	return distance(list_nodes_.begin(), std::find(list_nodes_.begin(), list_nodes_.end(), item));
}




            /*********************** Reels *********************************/

float Individual::compute(Node* aNode , const float var_value){

	if ((aNode -> left_ == nullptr) && (aNode -> right_ == nullptr)) { //If the left or right node is empty -> content is a variable or a constant

    if (aNode -> type_ == 4){ //If it is a constant, we convert the string into a float
      return stof(aNode -> content_);
    }

    if (aNode -> type_ == 3) { //If it is a variable, we replace it by the var value
      return var_value;
    }
  }

  else if ((aNode -> left_ != nullptr) && (aNode -> right_ == nullptr)) { // content is a simple_op
    float left_value = compute(aNode ->left_, var_value);

    if(aNode -> content_ == "cos"){
	    return cos(left_value);
    }
    if(aNode -> content_ == "sin"){
	    return sin(left_value);
    }
    if(aNode -> content_ == "exp"){
	    return exp(left_value);
    }
    if(aNode -> content_ == "ln"){
    	if (left_value<=0){ 
    		return FLT_MAX;
    	}
	    else{
	    	return log(left_value);
	    }
    }
    if(aNode -> content_ == "sqrt"){ 
    	if (left_value<0){
    		return FLT_MAX;
    	}
	    else{
	    	return sqrt(left_value);
	    }

    }
    if(aNode -> content_ == "abs"){
	    return abs(left_value);
    }

}

else if ((aNode -> left_ != nullptr) && (aNode -> right_ != nullptr)) { // content is a double_op
    float left_value = compute(aNode ->left_, var_value);
    float right_value = compute(aNode ->right_, var_value);

   if(aNode -> content_ == "+"){
	    return left_value + right_value;
    }

    if(aNode -> content_ == "-"){
	    return left_value - right_value;
    }
    if(aNode -> content_ == "*"){
	    return left_value * right_value;
    }
    if(aNode -> content_ == "/"){ 	
    	if (right_value==0){
    		return FLT_MAX;
    	}
	    else{
	    	return left_value / right_value;
	    }
    }
  }
}


void Individual::calculate_Fitness(const std::vector<float> vect_var, const std::vector<float> vect_obs){
    fitness_ = 0;
    std::vector<float> valformula;
    for (size_t i=0; i<vect_var.size(); i++){
        valformula.push_back(compute(root_, vect_var.at(i)));
    }

    for (size_t j=0; j<vect_var.size(); j++){
    	if (fitness_ < FLT_MAX){
    		fitness_ += pow(vect_obs.at(j) - valformula.at(j),2);
    	}else{
    		break;
    	}
    }


}

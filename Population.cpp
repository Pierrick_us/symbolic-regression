#include "Node.h"
#include "Individual.h"
#include "Population.h"
#include <vector>
#include <string>
#include <list>
#include <iostream>

using std::vector;
using std::cout;
using std::endl;

/****** Static Attributes *********/

vector<string> Population::simple_bool_operators_ = {"!"};
vector<string> Population::double_bool_operators_ = {"&&", "||"};
vector<string> Population::bool_const_ = {"0", "1"};


vector<string> Population::simple_real_operators_ = {"cos","sin","exp","ln","sqrt","abs"};
vector<string> Population::double_real_operators_ = {"+", "-","*","/"};
vector<string> Population::real_variables_ = {"x"};
vector<string> Population::real_const_ = {"1", "2", "3", "4", "5", "6", "7", "8", "9"}  ;

int Population::nb_generations_max_ = 1000;


// Constructor (booleans) :

Population::Population(int nb_Children, int nbr_var, float threshold, int nb_generations_user,  const vector<vector<bool>> mat_user, const vector<bool> obs) {
  srand((int)time(0));
  booleanCase_ = true;
  nb_Children_ = nb_Children;
  for (int i=0; i < nbr_var; ++i){  // initialisation of the variable vector
    variables_.push_back(std::to_string(i));
  }

  //vector of the const thant can be used
  threshold_fitness_= threshold;
  nb_generations_user_= nb_generations_user;

  matrice_user_ = mat_user;
  vect_obs_ = obs;
  parent_ = create_first_ind();
  parent_ -> calculate_Fitness(matrice_user_, vect_obs_);
  best_indiv_ = parent_ -> copy_indiv(parent_);
}


// Constructor (reals) :

Population::Population(int nb_Children, float threshold, int nb_generations_user, const vector<float> vect_var, const vector<float> vect_obs_real){ 
  srand((int)time(0));
  booleanCase_ = false;
  nb_Children_ = nb_Children;
  threshold_fitness_ = threshold;
  nb_generations_user_= nb_generations_user;

  vect_var_ = vect_var;
  vect_obs_real_ = vect_obs_real;

  parent_ = create_first_ind();
  parent_ -> calculate_Fitness(vect_var_,   vect_obs_real_ = vect_obs_real);
  best_indiv_ = parent_ -> copy_indiv(parent_);
}


// Destructor :


Population::~Population(){
  delete parent_;
  delete best_indiv_;
}


/********* Methods for Booleans ******************/


//create_first_ind() : function that creates the first individual of the
Individual* Population::create_first_ind(){
  Node* n = new Node ("1",nullptr,4);
  if (booleanCase_){
    Individual* first = new Individual (n,simple_bool_operators_ , double_bool_operators_ , variables_, bool_const_);
    first->insert_Node(0, first->root_);
    return first;
  }
  else {
    Individual* first = new Individual (n, simple_real_operators_, double_real_operators_, real_variables_, real_const_);
    first->insert_Node(0, first->root_);
    return first;
  }
}



void Population::find_best_indiv(){
  int counter=0;

  while ((counter < nb_Children_) && (best_indiv_ ->fitness_ > threshold_fitness_)){
    Individual child(*parent_);
    child.apply_mutation(); // we apply a mutation on the child
    
    if (booleanCase_){
      child.calculate_Fitness(matrice_user_, vect_obs_); // we update its fitness_
    }
    else {
      child.calculate_Fitness(vect_var_, vect_obs_real_);
    }

    if (child.fitness_ < best_indiv_->fitness_){
      best_indiv_ -> root_ =  best_indiv_ -> duplicate_Node(child.root_);
      best_indiv_ -> list_nodes_ = {};
      best_indiv_ -> fill_ListNode(best_indiv_ -> root_);
      best_indiv_ -> fitness_ = child.fitness_;
      
    }
    
    ++counter;
  }

}


// Create new generations while the fitness of the best individual is not lower than the threshold given by the user and return the best individual at the end.

void Population::final_indiv(){
  int nb_gen = 0;
  find_best_indiv();
  evolution_fitness_.push_back(best_indiv_ -> fitness_);
  ++nb_gen;

  if(nb_generations_user_ > nb_generations_max_){
    nb_generations_user_ = nb_generations_max_;
  }

  while ((nb_gen<nb_generations_user_) && (best_indiv_->fitness_ > threshold_fitness_)){
    parent_= parent_ -> copy_indiv(best_indiv_);
    find_best_indiv();
    evolution_fitness_.push_back(best_indiv_ -> fitness_);
    ++nb_gen;
  }
}


/*** Methods that will be used in python to get the important things after the simulation ******/

vector<float> Population::compute_values_vect(){
  vector<float> compute_values;
  if (booleanCase_){
    for (size_t i=0; i<matrice_user_.size(); i++){
      compute_values.push_back(best_indiv_ -> compute(best_indiv_ -> root_, matrice_user_.at(i)));
    }
    return compute_values;
  }
  else {
    for (size_t i=0; i<vect_var_.size(); i++){
      compute_values.push_back(best_indiv_ -> compute(best_indiv_ -> root_, vect_var_.at(i)));
    }
    return compute_values;
  }
}


string Population::formula(){
  return best_indiv_ -> get_formula(best_indiv_ -> root_);
}


float Population::fitness(){
  return best_indiv_ -> fitness_;

}


void Population::Print(){
  std::cout<<"********* HELLO WORLD !********* "<<std::endl;
}


vector<float> Population::evolution_fitness(){
  return evolution_fitness_;
}



int Population::convergence_fitness(){
  float best_fitness =  evolution_fitness_[evolution_fitness_.size() -1];
  size_t i = 0;
  while((evolution_fitness_[i] > best_fitness) && (i <  evolution_fitness_.size())) {
    ++i;
  }
  return i;
}

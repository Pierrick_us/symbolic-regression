#ifndef INDIVIDUAL_
#define INDIVIDUAL_

#include "Node.h"

#include <string>
#include <vector>

using std::vector;
using std::string;


class Individual{

friend class Population;

  protected:

  /***** Attributes *******/

  Node* root_;
  float fitness_;
  vector<Node*> list_nodes_;        // vector of all the Nodes* of an Individual
  vector<string> simple_operators_; //vector of all posible SIMPLE OPERATORS
  vector<string> double_operators_; //vector of all posible DOUBLE OPERATORS
  vector<string> variables_;        //vector of references to the variables entered by the user
  vector<string> const_;            //vector of all the constants thant can be use


  /******* protected Methods *******/

  Node* duplicate_Node(const Node* model);
  /**
  return : a Node* which is the pectect copy of the Node* given in parameter.
  Used in the copy constructor to copy the root_ and to get a copy of the tree.
  **/


  void fill_ListNode(Node* n);
  /**
  Used in the copy constructor to fill the vector list_nodes_ thanks to the root_ of an Individual and  its descendants Nodes.
  **/



  public:

  /*************************
  Constructors - Destructor
  *************************/

    Individual()=delete;
  	Individual(const Individual& model);
  	Individual(Node* start, vector<string> simple, vector<string> doubles,  vector<string> variable, vector<string> consts);
  	~Individual();

    /************
       Methods
    ***********/

    Individual* copy_indiv(const Individual* indiv);
    /**
    Create a copy of an Individual (similar of the copy-constructor)
    indiv : a pointer of an Individual
    return : a pointer of another Individual which is a copy of the one given in parameter
    **/



     void apply_mutation();
    /**
    Apply one the the three possible mutations on a random Node
    **/



    void delete_Node(int pos, Node* old);
    /**
    Used to delete the Node old from the tree which is at position pos in the vector list_nodes_
    Delete also the subtree below the Node old (method delete_branch)
    Replace this Node by a constant or a variable
    **/

	void delete_branch(Node* n);

	  /**
    Delete the subtree below the Node n
    Erase all the nodes of the subtree from the vector list_nodes_
    **/

    void insert_Node(int pos, Node* old);

    /**
    pos : position of the Node old in list_node_
    old : pointer to a  Node of an Individiual
    Insert a random Node before the Node old in the Individual
    **/


    void replace_Node(int pos, Node* old);
    /**
    pos : position of the Node old in list_node_
    old : pointer to a  Node of an Individiual which will be replace
    Replace of the Node old which is at the position pos in the vector list_nodes_
    The replacement Node is randomly selected between the simple operators, double operators, variables and constants
    **/


    string get_formula(Node* rootNode);
    /**
    rootNode : pointer to the root Node of an Individual
    Print the formula of an individual thanks to the infix course.
    We print content of each Node of the Individual in the right order.

    **/

    void calculate_Fitness(const std::vector<vector<bool>> matvar, const std::vector<bool> vect);
    //  Changes fitness_ value of the individual thanks to its formula

	bool compute(Node* rootNode, const std::vector<bool> line);

	// compute each node of the individual
	  /**
    Evaluate the formula of an Individual.
    return : a boolean which is the result value.
    **/

    int find_node(Node* item);
    /**
    Used to find the position of a Node given in paremeter
    return : the position (int) of the Node in the vector list_nodes_
    **/





    /*********  Reels ************/



    void calculate_Fitness(const std::vector<float> vect_var, const std::vector<float> vect_obs);

    //Reals: Calculates the fitness of the individual, taking in parameter the matrix and vector the user gave. It changes the attribute fitness_ of the individual


    float compute(Node* rootNode, const float var);



};


#endif //INDIVIDUAL_

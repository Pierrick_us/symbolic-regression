#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 23 10:10:12 2019

@author: cgonzalezg
"""
import numpy as np
import matplotlib.pyplot as plt
import my_wrapper_c as mwc

from sympy import simplify


def plot_model(my_x, my_y, model_val):
    #plot the best individual and the observations 

    plt.plot(my_x, my_y, "b:o", label="observations", c='lightblue')
    plt.plot(my_x, model_val, "b:o", label="observations", c='lightblue')

    
    plt.legend()
    plt.title("Visualization of the model and the observations")
    plt.xlabel("x")
    plt.ylabel("f(x)")
    plt.show()
    
def summary(obs,var):
    
    
def simple_formula(formula):
    return str(simplify(formula))
    
def plot_fitness_evolution(list_fitness):
        
a=[1.2,1.5,3]
b=[1,1.45,2.8]
v=[2,3,4]
plot_model(a,b,v)

#we genereta a population 
pop=generate_Pop()
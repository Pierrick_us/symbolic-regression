#include "Node.h"
#include "Individual.h"
#include "Population.h"

#include <iostream>
#include <vector>
#include <list>
#include <cfloat>

using std::cout;
using std::endl;
using std::vector;

int main(){
  srand((int)time(0));

  //-------------TEST REELS -------------------

/*
	vector<string> simple_op={"cos","sin","exp","l","sqrt","abs"};
  vector<string> double_op = {"+", "-","*","/"};
  vector<string> var = {"a", "b", "c"};
	vector<string> cons = {"0", "1"};

	Node* n1=new Node("+",nullptr,nullptr, nullptr,2);
	Node* n2=new Node("10",nullptr,nullptr, n1, 4);
	Node* n3=new Node("sqrt",nullptr,nullptr, n1, 1);
	Node* n4= new Node("x",nullptr,nullptr, n3, 3);

	n1->left_ = n2;
	n1->right_ = n3;
	n3->left_= n4;

	Individual* Ireal= new Individual(n1, simple_op, double_op, var, cons);
	Ireal->list_nodes_.push_back(n2);
  //cout << Ireal -> get_formula(Ireal -> root_) << endl;
	Ireal->list_nodes_.push_back(n3);
	Ireal->list_nodes_.push_back(n4);



  */
  /*



  cout << "" << endl;
	//Ireal->calculate_Fitness(vect_var,vect_obs);
  */
  cout << "theorical formula :  4x + 2*ln(x) " << endl;
	vector<float> vect_var1={0.5, 1.5, 6, 9, 11};
	vector<float> vect_obs1 ={2.81, 9, 29.78, 42.59, 50.99};


  cout << "theorical formula : 2*x+1 " << endl;
  vector<float> vect_var2={-2, -1, 0, 1.5, 6, 7, 10};
  vector<float> vect_obs2 ={-3, -1, 1, 4, 13, 15, 21};


  Population* P2 = new Population(100, 0.01, 100, vect_var1, vect_obs1);

  /** test de methof final_indiv()() *****/
  cout << "\n" << endl;
  P2 -> final_indiv();
  cout << "" << endl;

  /** test de methof formula() *****/
  cout << "formula() :" ;
  cout << P2 -> formula() << endl;

  /** test de methof fitness() *****/
  cout << "fitness() :" ;
  cout << P2 -> fitness() << endl;

  /** test de methof compute_values_vect() *****/
  cout << "\nVecteurs of computes values : " << endl;
  vector<float> vect_model_val = P2 -> compute_values_vect();
  for (size_t i =0; i<vect_model_val.size(); ++i){
    cout <<  "f("  << vect_var1[i] << ") = " << vect_model_val[i] << "  |  "  ;
  }

  /** test de methof fitness_evolution() *****/
 /*
  cout << "\nVecteurs of fitness values at the end of each generation : " << endl;
  vector<float> fitness_vect = P2 -> evolution_fitness();
  float best_fitness =  fitness_vect[fitness_vect.size() -1];
  size_t i = 0;
  while((fitness_vect[i] > best_fitness) && (i <  fitness_vect.size())) {
    ++i;
  }
*/

  /** test de methof fitness_convergence() *****/
  cout <<  "\nLa best fitness "  << P2 -> fitness()  << " est atteinte des la generation  " << P2 -> convergence_fitness() << endl;




  cout << "\nEND " << endl;



//	cout<<(Ireal->fitness_ < FLT_MAX)<<endl;


  //delete Ireal;
  delete P2;



/*
//------------ test Bool---------------//
vector<vector<bool>>my_booleans= {{1,1,1,0},{1,1,1,1},{0,0,1,0}};
vector<bool>my_obs={1,0,0};
Population p3(5,3,0.01,50,my_booleans,my_obs);
cout<<p3.formula()<<endl;
cout<<p3.fitness()<<endl;
*/


/*

	vector<string> simple_op = {"!"};
	vector<string> double_op = {"&&", "||"};
	vector<string> var = {"a", "b", "c"};
	vector<string> cons = {"0", "1"};

	//Creation of a vector and a matrix
	std::vector<vector<bool>> mat = {{true,false, true,true,false, false},{false, true, false, true,false, true},{true,true,false, true, true, false},{false, true,false,  true, true,false}, {true, true,true,false, false, false}, {false, true,false, true, false, false}, {true, true, false, false, false, false}, {false, true, true,false, true, false},{true, true, false, true, false, true},{true, false, false, true, true, true}, {false, true, true, true, false, false},{true, true, true, true, true, true}};
	std::vector<bool> myvect = {true, false, true, false, true, true, false, true, false, false, false, true};


	/****  Creation d'un Arbre manuellement ******/

 /*
	Node* n1=new Node("&&",nullptr,nullptr, nullptr,2);
	Node* n2=new Node("0",nullptr,nullptr, n1, 3);
	Node* n3=new Node("!",nullptr,nullptr, n1, 1);
	Node* n4= new Node("2",nullptr,nullptr, n3, 3);

	n1->left_ = n2;
	n1->right_ = n3;

	n3->left_=n4;

	Individual* i1= new Individual(n1, simple_op, double_op, var, cons);
	i1->list_nodes_.push_back(n2);
	i1->list_nodes_.push_back(n3);
	i1->list_nodes_.push_back(n4);


/*
	  Individual* i2 = i1 -> copy_indiv(i1);
	  cout << " \n adresse   i1 : " <<  i1 << " |  adresse   i2 :" << i2 << endl;
	   cout << "  adresse root   i1 : " <<  i1 -> root_ << " |  adresse root   i2 :" << i2-> root_ << endl;
	   cout << i2 -> get_formula(i2->root_) << endl;
	   cout << i2 -> list_nodes_.size()  << endl;

	   cout << i2 -> get_formula(i2->root_) << endl;



	   int x = 3;
	   if (x == 3){
	   	i2 = i1;
	   }

	   cout <<  "adresse i1 : " <<  i1 << " et sa formule : " << i1-> get_formula(i1->root_) << endl;
	   cout <<  "adresse i2 : " <<  i1 << " et sa formule : " << i2-> get_formula(i2->root_) << endl




    int i = 0;
    while  ( i<2){
        Individual i1_cop(*i1);
        Individual* i1_copy = &i1_cop;
        cout << " \n adresse  root i1 : " <<  i1 -> root_ << " |  adresse root  i1_copy :" << i1_copy -> root_ << endl;
        ++i;
    }

    */

/*


   cout << " on delete i1 " << endl;

   delete i1;

   cout << " \n " << endl;

  /**
  cout << i1 -> get_formula(i1 -> root_) << endl;
 	i1 -> calculate_Fitness(mat, myvect);

  	delete i1;
	  cout << "\n" << endl;
**/


  /****  Creation d'une Population******/



/*

	Population* P1 = new Population(10, 3, 2, mat, myvect);
  cout << "\n" << endl;
	//P1 -> find_best_indiv();




	 P1 -> final_indiv();
*/

/*
  cout << "\n" << endl;

  cout << "on le delete le final_indiv" << endl;

  delete P1;
	//delete best;
	//delete final_indiv;






  /*
  for (int i = 0; i < 5; ++i){

    P1.parent_ -> apply_mutation();
    cout << "La formule du parent  est :  " << P1.parent_ -> get_formula(P1.parent_ -> root_)  << endl;
    P1.parent_-> calculate_Fitness(mat, myvect);
  }
  */






  //P1.find_best_indiv();

  /**
	string s = P1.final_indiv() -> get_formula(P1.final_indiv() -> root_);
  cout << s << endl;







	cout << "La formule de l'individu i1 est : " << endl;
	string s2 = i1 -> get_formula(i1 -> root_);
	cout << s2 << endl;

    cout << i1 -> compute(i1 -> root_, myvect) << endl;
    i1 -> calculate_Fitness(mat,myvect);
    cout << i1 -> fitness_ << endl;

   **/



	/*
	// Creation d'une copie l'individu i1
	cout << "On fait une copie de i1 appelee i1_copy " <<  endl;
	Individual i1_copy(*i1);

  	cout << "Les deux arbres sont bien distincts : " << endl;
  	cout << "Adresse de i1 : " << i1 << endl;
  	cout << "Adresse de i1copy " << &i1_copy << endl;

  	cout << "La formule de la copie de i1 est bien la même :  " ;
	i1_copy.get_formula(i1_copy.root_);
	cout << "\n" << endl;


	// On fait muter i1
 	 cout << "On fait muter i1 :" ;
	i1->apply_mutation();
	cout << "\n" << endl;

	cout << "La nouvelle formule de i1 est : " ;
	i1->get_formula(i1->root_);
	cout << "\n" << endl;

	cout << "La formule de la copie de i1  n'a cependant pas changee:  " ;
  	i1_copy.get_formula(i1_copy.root_);
	cout << "\n" << endl;


	//Premiers tests sur population
	Population p(3,3,3);
	cout<<*(p.simple_operators_.begin())<<endl;
	p.parent_->get_formula(p.parent_->root_);
	cout << "\n" << endl;

	//for
	for(int i=0; i<5;++i){
		i1->apply_mutation();
		i1->get_formula(i1->root_);
		cout<<"\n"<<endl;
	}
	/*
	i1->get_formula(i1->root_);
	cout << "" << endl;

	i1->get_formula(i1->root_);
	cout << "" << endl;


	int x=i1.find_node(n2);
	cout<< x <<endl;




	i1->apply_mutation();
	i1->get_formula(i1->root_);
	cout << "" << endl;

	// i1.delete_Node(0,i1.root_);
	//i1.delete_branch(n1);
	delete i1;
	//i1->get_formula(i1->root_);
	cout << "" << endl;

	Individual i2(&n2, simple_op, double_op, var, cons);


		i1.apply_mutation();
		i1.get_formula(i1.root_);
			cout << "" << endl;
	*/

	/*
	srand((int)time(0));
		for (int i=0;i<10;++i){

			std::cout<<rand()%2<<std::endl;
	}
	*/

	/*
	Individual i(&n);


	cout<< (i.get_nb_Nodes()== 0) <<endl;

	cout<< (i.get_fitness() == 0) <<endl;
	cout<<"Test : get_list_nodes() : "<<endl;
	cout<<( i.get_list_nodes()[0]==n)<<endl;
	cout<<"Test : erase(Node* n)"<<endl;
	i.erase_node(n);
	cout<<(i.get_list_nodes().size()==0)<<endl; // check the new size of ou list of nodes : must be empty normally



	cout<<"******TESTS ON POPULATION CLASS******"<<endl;
	vector <bool> observations = {1,1,1,1,1,1,1,1,1};
	Population p(0,observations);
	cout<<"Test : get_index()"<<endl;
	cout<< (p.get_index() == 0) << endl;
	cout<<"Test : get_nb_indiv()"<<endl;
	cout<< (p.get_nb_Indiv() == 0) << endl;
	cout<<"Test : get_obs()"<<endl;
	cout<<(p.get_obs()==observations) <<endl;
	cout<<"Test : create_first_ind() : "<<endl;
	cout<<(p.create_first_ind()->get_list_nodes()[0]->get_content()=="x")<<endl;
	//delete p.create_first_ind();
	//delete p.create_first_ind()->get_list_nodes()[0];



	Individual i2(i);
	cout<<(i2.get_nb_Nodes()==i.get_nb_Nodes())<<endl;
	cout<<(i2.get_fitness()==i.get_fitness())<<endl;

	Population p(0);
	cout<< (p.get_nb_Indiv() == 0) << endl;


	Population p(50, &i);
	cout<< (p.get_nb_Children() == 50) << endl;
	*/

/*

	std::cout << "\n" << "Appel du destructeur "  <<  std::endl;

*/



  return 0;
}

